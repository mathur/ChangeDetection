﻿using UnityEngine;
using System.Collections;

public class QuestionText : MonoBehaviour
{

	public void BlankOut () {
        GetComponent<TextMesh>().text = "";
    }


    public void EndManagement()
    {
        GetComponent<TextMesh>().text = "Thank you! Test complete. :)";
        Debug.Log("Thank you! Test complete. :)");
        ApplicationQuit(5.5f); // Quit after 5 and a half seconds
    }

    public void AskQuestion()
    {
        GetComponent<TextMesh>().text = "What did you see?\n[1] Objects changed colour.\n[2] Objects did not change colour";
    }

    IEnumerator ApplicationQuit(float time)
    {
        yield return new WaitForSeconds(time);

        Application.Quit();
    }
}
