﻿//Last minute
//Definitely sub-optimal

using UnityEngine;
using System.Collections;

public class TextSwitchFirstCase : MonoBehaviour {
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (ControllerMaster.amIEnabled)
        {
            GetComponent<TextMesh>().text = "Look directly at the fixation cross.\nPress the spacebar when ready to start..\nYour task is to detect if object(s) change colour.";
            GetComponent<TextMesh>().color = Color.black;
        }
    }
}
