﻿//Picks up user input
//Uses Placer3D's state
using UnityEngine;

public class UserInputHandler : MonoBehaviour
{
    public void One() //Function mapping from radial button click set up in inspector
    {
        if(Placer3D.state == 4)
            gameObject.SendMessage("RegisterAnswer", 1, SendMessageOptions.RequireReceiver);
    }

    public void Two()
    {
        if (Placer3D.state == 4)
            gameObject.SendMessage("RegisterAnswer", 2, SendMessageOptions.RequireReceiver);
    }

    public void Three()
    {
        if (Placer3D.state == 4)
            gameObject.SendMessage("RegisterAnswer", 3, SendMessageOptions.RequireReceiver);
    }

    public void Four()
    {
        if (Placer3D.state == 4)
            gameObject.SendMessage("RegisterAnswer", 4, SendMessageOptions.RequireReceiver);
    }

    private void Update()
    {
        if (Placer3D.state == 4)
        {
            if(Input.GetKeyDown(KeyCode.Alpha1))
            {
                gameObject.SendMessage("RegisterAnswer", 1, SendMessageOptions.RequireReceiver);
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                gameObject.SendMessage("RegisterAnswer", 2, SendMessageOptions.RequireReceiver);
            }
        }
    }
}
