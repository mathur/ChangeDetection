﻿//Attached to the master gameobject
//Uses Placer3D.state and LookingAtFixationPlane.valid
//Right = yes, Left = no
using UnityEngine;
using System.Collections;
using System.IO;


public class ControllerMaster : MonoBehaviour
{
    public GameObject auxiliaryText; //Reference to the G.O. that has text for questions and ending -> set in the inspector
    private QuestionText qt;

    public GameObject focusBox; //Reference set in inspector

    public static bool amIEnabled = false;
    public static bool testOver; //Is the test complete?? -> Set by Placer3D

    public static SceneDescription currScene;  //Set by Placer3D


    private string resultFileName; //What's the filename?
    private StreamWriter writer;

    public static GameObject[] toBeDisabled; //Set in Start()

    private int deviceIndexR; //We only use one hand controller

    private static string nameOfUser;

    public bool flatScreen;
    public static bool is2D;

    // Use this for initialization
    void Start()
    {

        toBeDisabled = GameObject.FindGameObjectsWithTag("Fixation"); //make an array of stuff to quickly disable/enable

        //file creation--- boring stuff
        nameOfUser = (System.Environment.GetCommandLineArgs()).Length > 1 ? (System.Environment.GetCommandLineArgs())[1] : "DummyUser";

        string  identifierForTest = nameOfUser; //Maybe add some other shit?

        resultFileName = identifierForTest + "_" + System.DateTime.Now.ToString("yyyy-dd-M--HH-mm") + ".csv";
        writer = new StreamWriter(resultFileName);
        writer.AutoFlush = true;
        //Add the header
        writer.WriteLine("SubjectID, TrialNum, NumInF, ChangeInF, BoolChange, BoolCorrect, Answer, Timestamp");
        
        qt = auxiliaryText.GetComponent<QuestionText>();
        //focusBox.SetActive(false);

        //Set the static bool so that flat screen version can be performed
        is2D = flatScreen;
    }

    // Update is called once per frame
    void Update()
    {
        deviceIndexR = SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Rightmost); // Done again and again to keep track of just woken up cotrollers

        if ((Placer3D.state == 0 && LookingAtFixationPlane.valid && !testOver)) //user looking at fixation plane
        {
            if(is2D)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    if (amIEnabled)
                    {
                        for (int i = 0; i < toBeDisabled.Length; i++)
                            toBeDisabled[i].SetActive(false);

                        Placer3D.state = 1; //Start the placement 
                    }
                    else
                        amIEnabled = true;
                }
            }

            else if (!is2D)
            {
                if ((deviceIndexR != -1 && (SteamVR_Controller.Input(deviceIndexR).GetPressUp(SteamVR_Controller.ButtonMask.Grip))))
                {
                    if (amIEnabled)
                    {
                        for (int i = 0; i < toBeDisabled.Length; i++)
                            toBeDisabled[i].SetActive(false);

                        Placer3D.state = 1; //Start the placement 
                    }
                    else
                        amIEnabled = true;
                }
            }

        }
        else if (Placer3D.state == 4)
        {
            //ask question
            //focusBox.SetActive(false);
            qt.AskQuestion();
        }


    }

    void OnApplicationQuit()
    {
        writer.Close();
    }


    void RegisterAnswer(int choice)
    {
        Placer3D.state = 100; //wait

        string answerInF="err";


        bool change = currScene.changeInFocus > 0 ? true : false; //Was there a change in colour?
        bool correct = false;
        switch (choice) //Resolve correctness of choice
        {
            case 1: //Yes
                answerInF = change ? "Hit" : "FalseAlarm";
                correct = change ? true : false;
                break;
            case 2: //No
                answerInF = change ? "Miss" : "CorrectRejection";
                correct = change ? false : true;
                break;

            default:
                Debug.LogError("Unknown choice selected by user");
                break;
        }
       
                            //"SubjectID, TrialNum, NumInF, ChangeInF, BoolChange, BoolCorrect, Answer, Timestamp"
        writer.WriteLine(nameOfUser + ", " + currScene.trialNumber + ", " + currScene.numInFocus + ", " + currScene.changeInFocus + ", " + change.ToString() + ", " + correct.ToString() + ", " + answerInF + ", " + System.DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss"));
        Debug.Log(nameOfUser + ", " + currScene.trialNumber + ", " + currScene.numInFocus +  ", " + currScene.changeInFocus + ", " + ", " + answerInF);
        qt.BlankOut(); //Blankout whatever was there on the question screen


        if (!testOver)
        {
            //Enable the fixation screen
            for (int i = 0; i < toBeDisabled.Length; i++) //Set fixation stuff active
                toBeDisabled[i].SetActive(true);
            Placer3D.state = 0; //Set state to 0 as answer has been registered
        }
        else
        {
            qt.EndManagement();
        }
    }



}
