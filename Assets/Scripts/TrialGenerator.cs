﻿//Aim is to place objects randomly around the surface of a sphere

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

[System.Serializable]
public class ObjectDescription
{
    public Vector3 position; //position of object
    public bool shape; //true for cube, false for rect
    public Color colour; 
    public Vector3 sphericalCoord;
    private static Color[] colourArray = new Color[4] {Color.red, Color.blue, Color.red, Color.blue};
    public ObjectDescription (Vector3 pos, bool shp, Vector3 sphCoord, Color givenColour)
    {
        position = pos;
        shape = shp;
        colour = givenColour;
        sphericalCoord = sphCoord;
       
    }
    public ObjectDescription(ObjectDescription obj)
    {
        position = obj.position;
        shape = obj.shape;
        colour = obj.colour;
        sphericalCoord = obj.sphericalCoord;
    }
    public void ChangeColour()
    {
        Color newColour;
        do
        {
            newColour = colourArray[Random.Range(0, 4)];
        } while (newColour == colour);


        colour = newColour; //Assign this as the new colour

    }

    public void ChangeShape(ObjectDescription obj)
    {
        shape = obj.shape ? false : true; //Invert shape
    }
}

[System.Serializable]
public class SceneDescription //wrapper for the scene
{
    public int trialNumber;
    public int numInFocus; //num of objects in focus

    public int changeInFocus;

   
    public List<ObjectDescription> objectsBefore; //list of all objects
    public List<ObjectDescription> objectsAfter;

    public SceneDescription(int counter, int nFocus, int cFocus)
    {
        trialNumber= counter;
        numInFocus = nFocus;
        changeInFocus = cFocus;

        objectsBefore = new List<ObjectDescription>();
        objectsAfter = new List<ObjectDescription>();
    }
}



public class TrialGenerator : MonoBehaviour {

    //Object placement vars
    public float maxDistance = 5.881f;
    public float minDistance = 4.119f;
    public float minDistanceBetweenObjects = 0.22f; //This is used to avoid collisions and occlusions

    private int numOfTrials = 1; //start with a 1 from Trial Line Number

    //Overall FOV
    private static float polarRange = 10f;
    private static float elevationRange = 10f;

    private static int[,] nCFocus;// = new int[,] { { 2, 0 }, { 2, 1 }, { 3, 0 }, {3, 1}, { 3, 2 }, { 4, 0 }, { 4, 0 }, { 4, 1 }, { 4, 2 }, { 5, 0 }, { 5, 0 }, { 5, 1 }, { 5, 2 }, { 5, 3 }, { 6, 0 }, { 6, 0 }, { 6, 0 }, { 6, 1 }, { 6, 2 }, { 6, 3 }, { 7, 0 }, { 7, 0 }, { 7, 0 }, { 7, 1 }, { 7, 2 }, { 7, 3 }, { 7, 4}, { 8, 0 }, { 8, 0 }, { 8, 0 }, { 8, 0 }, { 8, 1 }, { 8, 2 }, { 8, 3 }, { 8, 4 }, { 9, 0 }, { 9, 0 }, { 9, 0 }, { 9, 0 }, { 9, 1 }, { 9, 2 }, { 9, 3 }, { 9, 4 }, { 9, 5 }, { 10, 0 }, { 10, 0 }, { 10, 0 }, { 10, 0 }, { 10, 0 }, { 10, 1 }, { 10, 2 }, { 10, 3 }, { 10, 4 }, { 10, 5 }, { 11, 0 }, { 11, 0 }, { 11, 0 }, { 11, 0 }, { 11, 0 }, { 11, 1 }, { 11, 2 }, { 11, 3}, {11, 4}, {11, 5}, { 11, 6 }, { 12, 0 }, { 12, 0 }, { 12, 0 }, { 12, 0 }, { 12, 0 }, { 12, 0 }, { 12, 1 }, { 12, 2 }, { 12, 3 }, { 12, 4 }, { 12, 5 }, { 12, 6 }, { 13, 0 }, { 13, 0 }, { 13, 0 }, { 13, 0 }, { 13, 0 }, { 13, 0 }, { 13, 1 }, { 13, 2 }, { 13, 3 }, { 13, 4 }, { 13, 5 }, { 13, 6 }, { 13, 7 } };
    //Set in the Awake() function

    void Awake()
    {

        nCFocus = new int[200, 2]; //Make it arbitrarily large
        int arrayCounter = 0;
        for (int i = 2; i < 14; i++) //From n= 2 to 13 
        {
            for (int j = 1; j < (2 * i - 1); j++) //From k=1 to k=n-1 and several cases where k=0
            {
                if (j < i)
                {
                    nCFocus[arrayCounter, 0] = i;
                    nCFocus[arrayCounter, 1] = j;
                    arrayCounter++;
                }
                else
                {
                    nCFocus[arrayCounter, 0] = i;
                    nCFocus[arrayCounter, 1] = 0;
                    arrayCounter++;
                }
                Debug.Log(nCFocus[arrayCounter - 1, 0] + ", " + nCFocus[arrayCounter - 1, 1]);
            }
        }


        Debug.Log("We started generation at " + System.DateTime.Now);
        StreamWriter writer = new StreamWriter("AllTrials.txt"); //Config file

        int trialCounter = 1; //Current number of trial

      
        //foreach in focusNumbersOfObjects
        for (int i = 0; i < arrayCounter; i++)
        {
            for (int rep = 0; rep < 18; rep++) //Repeat some times
            {
                //for 2D-3D switch
                for (int choose3D = 0; choose3D < 1; choose3D++)
                {
                    int nFocus = nCFocus[i, 0];
                    int cFocus = nCFocus[i, 1];

                    //Initialize some variables and structures
                    int objectCounterF = 0;

                    //Keeps track of what has been included and what not
                    SceneDescription currScene = new SceneDescription(trialCounter, nFocus, cFocus);
                    do
                    {
                        bool cannotUseRand = false; //reset
                        Vector3 position, chosenSpherical;
                        if (choose3D > 0)
                            chosenSpherical = new Vector3(Random.Range(minDistance, maxDistance), Random.Range(Mathf.Deg2Rad * (-polarRange), Mathf.Deg2Rad * (polarRange)), Random.Range(Mathf.Deg2Rad * (-elevationRange), Mathf.Deg2Rad * (elevationRange)));
                        else
                            chosenSpherical = new Vector3(5f, Random.Range(Mathf.Deg2Rad * (-polarRange), Mathf.Deg2Rad * (polarRange)), Random.Range(Mathf.Deg2Rad * (-elevationRange), Mathf.Deg2Rad * (elevationRange)));
                        position = ToCartesian(chosenSpherical);

                        //Is the newly initialized object usable? (does it satisfy minDistanceBetweenObjects)
                        for (int id = 0; id < currScene.objectsBefore.Count; id++) //TODO- Can be made smarter
                        {
                            if ((Mathf.Abs(currScene.objectsBefore[id].position.x - position.x) < minDistanceBetweenObjects) && (Mathf.Abs(currScene.objectsBefore[id].position.y - position.y) < minDistanceBetweenObjects) && (Mathf.Abs(currScene.objectsBefore[id].position.z - position.z) < minDistanceBetweenObjects))
                            {//minDistanceBetweenObjects is not maintained
                                cannotUseRand = true;
                                break;
                            }
                        }
                        if (cannotUseRand) //Catch non satisfaction to start a new loop
                            continue;

                        //Actual instantiation of object
                        bool cubeOrRect = choose3D > 0 ? true : false;
                        Color clr = objectCounterF % 2 == 0 ? Color.blue : Color.red;
                        ObjectDescription newObj = new ObjectDescription(position, cubeOrRect, chosenSpherical, clr);
                        currScene.objectsBefore.Add(newObj);
                        objectCounterF++;

                        ObjectDescription changedObject = new ObjectDescription(newObj);
                        if (cFocus > 0)
                        {
                            changedObject.ChangeColour();
                            cFocus--; //decrement changes yet to do
                        }
                        currScene.objectsAfter.Add(changedObject); //Add the 'after' version


                        //increment helper variable for the loop
                    } while (objectCounterF < nFocus); //Until the counters are smaller

                    trialCounter++; //A new trial was added
                    writer.WriteLine(JsonUtility.ToJson(currScene, false)); //Add the trial to config file
                }
            }
                //loops
        }
        Debug.Log("We ended generation at " + System.DateTime.Now);
        writer.Close(); //close the file
    } //All placements are done, continue..
    
    public Vector3 ToCartesian (Vector3 spherical)
    {

        float a = spherical.x * Mathf.Cos(spherical.z);
        return new Vector3(a * Mathf.Cos(spherical.y), spherical.x * Mathf.Sin(spherical.z), a * Mathf.Sin(spherical.y));

    }
}