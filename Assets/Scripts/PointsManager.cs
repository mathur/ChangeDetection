﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsManager : MonoBehaviour {

    public static int points;

	// Use this for initialization
	void Start () {
        points = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CorrectFocusAnswer()
    {
        points += 100;
    }

    public void CorrectPeripheryAnswer()
    {
        points += 20;
    }

    public void IncorrectFocusAnswer()
    {

    }

    public void IncorrectPeripheryAnswer()
    {

    }
}
