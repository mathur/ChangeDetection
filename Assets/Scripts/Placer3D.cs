﻿//I place 3D objects!! :)

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class Placer3D : MonoBehaviour {
    public static bool blockManager = true;

    public static short state; //state of the placer

    public GameObject[] shapes; //Prefabs to cube and sphere attached in the inspector 

    
    private GameObject[] objectArray; //data structure used to store G.O.s
   
    private static int numTrials;
        
    
    private int randomIndex=0;
    private List<SceneDescription> ourExperiment;

	// Use this for initialization
	void Start () {

        state = 0; //init state

        Debug.Log("We start reading at " + System.DateTime.Now);

        StreamReader reader;

        //Give a value to firstBlock depending on the argument passed 
        if(System.Environment.GetCommandLineArgs().Length > 2 )
        {
            string arg = System.Environment.GetCommandLineArgs()[2];
            blockManager = arg.StartsWith("1")? true : false;
        }
        
        if (blockManager)
            reader = new StreamReader("SelectedTrials_mod.txt"); //Block-1 file
        else
            reader = new StreamReader("SelectedTrials_mod.txt"); //Block-2 file

        //Our Experiment
        ourExperiment = new List<SceneDescription>(); //Init the list

        //Read the first JSON object
        string readData = reader.ReadLine(); //1
               
        //Then, read in a loop   
        while (readData != null) //1
        {
            ourExperiment.Add(JsonUtility.FromJson<SceneDescription>(readData));
            readData = reader.ReadLine();
        }
     

        Debug.Log("Number of trials retrieved are " + ourExperiment.Count.ToString());

        Debug.Log("We end reading at " + System.DateTime.Now);

    }
	
	// Update is called once per frame
	void Update () {

        //state == 0 means waiting on fixation

        if (state == 1) //Time to display stuff
        {

            randomIndex = Random.Range(0, ourExperiment.Count); //chose a random trial number

            Debug.Log("Started a new trial with " + ourExperiment[randomIndex].numInFocus + " focus objects.");

            //Set some public static variables in the master according to the trial picked out
            ControllerMaster.currScene = ourExperiment[randomIndex];
            //Set relevant variables that are also used by the master

            objectArray = new GameObject[ourExperiment[randomIndex].objectsBefore.Count]; //Array initialization with size

            //Now, let us start the placement
            for (int i = 0; i < ourExperiment[randomIndex].objectsBefore.Count; i++)
            {
                Quaternion rot = Quaternion.identity;
                //create object
                if (ourExperiment[randomIndex].objectsBefore[i].shape) //True= cube, false = quad
                    objectArray[i] = Instantiate(shapes[0], ourExperiment[randomIndex].objectsBefore[i].position + new Vector3(0, LookingAtFixationPlane.height, 0), rot) as GameObject;
                else
                {
                    rot.eulerAngles = new Vector3(0, 90, 0); //As rects need to face the correct way
                    objectArray[i] = Instantiate(shapes[1], ourExperiment[randomIndex].objectsBefore[i].position + new Vector3(0, LookingAtFixationPlane.height, 0), rot) as GameObject;
                }
                //set colour
                objectArray[i].GetComponent<Renderer>().material.color = ourExperiment[randomIndex].objectsBefore[i].colour;
            }
            Debug.Log("Successfully placed " + objectArray.Length.ToString() + " before objects");
            state = 2; //Once, everything is done, set state to two -> displayed everything
            StartCoroutine(BeforeToBlank());
        }

        else if (state == 2) //Blanking time
        {
            if (objectArray.Length > 0)
            {
                foreach (GameObject go in objectArray)
                    DestroyObject(go);
            }
            StartCoroutine(BlankToAfter());
        }

        else if (state == 3) //AFTER time
        {
            objectArray = new GameObject[ourExperiment[randomIndex].objectsAfter.Count];
            for (int i = 0; i < ourExperiment[randomIndex].objectsAfter.Count; i++)
            {
                Quaternion rot = Quaternion.identity;
                //create object                 //Before and after shapes are the same
                if (ourExperiment[randomIndex].objectsBefore[i].shape) //True= cube, false = quad
                    objectArray[i] = Instantiate(shapes[0], ourExperiment[randomIndex].objectsAfter[i].position + new Vector3(0, LookingAtFixationPlane.height, 0), rot) as GameObject;
                else
                {
                    rot.eulerAngles = new Vector3(0, 90, 0); //As rects need to face the correct way
                    objectArray[i] = Instantiate(shapes[1], ourExperiment[randomIndex].objectsAfter[i].position + new Vector3(0, LookingAtFixationPlane.height, 0), rot) as GameObject;
                }
                //set colour
                objectArray[i].GetComponent<Renderer>().material.color = ourExperiment[randomIndex].objectsAfter[i].colour;
            }
            Debug.Log("Successfully placed " + objectArray.Length.ToString() + " after objects");
            StartCoroutine(AfterToQuestion());

        }


        else if (state == -1) //clean-up time
        {
            //Delete everything
            if (objectArray.Length > 0)
            {
                foreach (GameObject go in objectArray)
                    DestroyObject(go);
            }

            ourExperiment.RemoveAt(randomIndex);
            if (ourExperiment.Count == 0)
                ControllerMaster.testOver = true;

            state = 4; //wait for answer to question
        }
	}

    IEnumerator BeforeToBlank()
    {
        state = 100;
        yield return new WaitForSeconds(3.0f);
        state = 2;
    }

    IEnumerator BlankToAfter()
    {
        state = 100;
        yield return new WaitForSeconds(2.0f);
        state = 3;
    }
    IEnumerator AfterToQuestion()
    {
        state = 100;
        yield return new WaitForSeconds(3.0f);
        state = -1;
    }
   
}
