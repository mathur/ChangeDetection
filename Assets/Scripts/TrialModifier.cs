﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class TrialModifier : MonoBehaviour {
    private Color col1 = Color.blue, col2 = Color.red;
    private float maxDistance = 5.881f;
    private float minDistance = 4.119f;
    private float meanDistance = 5f;
    void Awake()
    {

        StreamReader reader;
        
        reader = new StreamReader("AllTrials.txt");

        //Our Experiment
        List<SceneDescription> ourExperiment = new List<SceneDescription>(); //Init the list

        //Read the first JSON object
        string readData = reader.ReadLine(); //1

        //Then, read in a loop   
        while (readData != null) //1
        {
            ourExperiment.Add(JsonUtility.FromJson<SceneDescription>(readData));
            readData = reader.ReadLine();
        }
        Debug.Log("Number of trials retrieved are " + ourExperiment.Count.ToString());
        reader.Close();

        //now open Mode file
        reader = new StreamReader("Mode.txt");

        //Our Experiment
        List<int> modes = new List<int>(); //Init the list

        //Read the first JSON object
        readData = reader.ReadLine(); //1

        //Then, read in a loop   
        while (readData != null) //1
        {
            modes.Add(int.Parse(readData));
            readData = reader.ReadLine();
        }
        Debug.Log("Number of modes retrieved are " + modes.Count.ToString());
        reader.Close();

        //Segment selected trials
        List<SceneDescription> selectedTrials = new List<SceneDescription>(); //Init the list
        foreach (int m in modes)
        {
            foreach (SceneDescription sd in ourExperiment)
            {
                if (sd.trialNumber == m)
                {
                    selectedTrials.Add(sd);
                    break;
                }
            }
        }
        //Write result
        StreamWriter wr = new StreamWriter("SelectedTrials.txt");
        foreach(SceneDescription sd in selectedTrials)
        {
            wr.WriteLine(JsonUtility.ToJson(sd));
        }
        wr.Close();
        Debug.Log("SelectedTrials written.");
        int i = 0;
        //Change shape, colour and add offset to 3D case
        foreach (SceneDescription sd in selectedTrials)
        {
            //Change alternating shape for before objects an add an offset
            if (i % 2 == 0)
            {
                for(int id = 0; id < sd.objectsBefore.Count; id ++)
                {
                    sd.objectsBefore[id].ChangeShape(sd.objectsBefore[id]); //change shape
                    sd.objectsAfter[id].ChangeShape(sd.objectsAfter[id]);

                    sd.objectsBefore[id].position.x = Random.Range(minDistance, maxDistance); //Add offset
                    sd.objectsAfter[id].position.x = sd.objectsBefore[id].position.x;
                    sd.objectsAfter[id].colour = sd.objectsBefore[id].colour; //Set colour as equal as change is done later
                }

            }
            else
            {
                for (int id = 0; id < sd.objectsBefore.Count; id++) //Remove rounding errors
                {
                    sd.objectsBefore[id].position.x = meanDistance;
                    sd.objectsAfter[id].position.x = sd.objectsBefore[id].position.x;
                    sd.objectsAfter[id].colour = sd.objectsBefore[id].colour; //Set colour as equal as change is done later
                }
            }
            //change the 'after objects' appropriately
            int n = sd.numInFocus, c = sd.changeInFocus, changedRToB, changedBToR;


            changedRToB = c / 2;
            changedBToR = c - changedRToB; //Set number of R & B's to be switched
           

            //changes blue to red

            foreach (ObjectDescription obj in sd.objectsAfter)
            {
                if ((obj.colour == col1) && changedBToR > 0)
                {
                    obj.colour = col2;
                    changedBToR--;
                    break;
                }
                else if ((obj.colour == col2) && changedRToB > 0) 
                {
                    obj.colour = col1;
                    changedRToB--;
                    break;
                }
            }
            
            i++;

        }
        wr = new StreamWriter("SelectedTrials_mod.txt");
        foreach (SceneDescription sd in selectedTrials)
        {
            wr.WriteLine(JsonUtility.ToJson(sd));
        }
        wr.Close();
        Debug.Log("SelectedTrials_mod written.");
    }

}
